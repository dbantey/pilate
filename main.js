 
const SHA256 = require('crypto-js/sha256');

class Block {

    constructor(timestamp, transactions, previousHash = '') {
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.previousHash = previousHash;

        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash() {
        return SHA256(
            this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce
        ).toString();
    }

    mineBlock(difficulty) {
        // Proof of work
        while (this.hash.substring(0, difficulty) !== Array(difficulty + 1).join('0')) {
            this.nonce++;
            this.hash = this.calculateHash();
        }
        console.log('Block mined: ' + this.hash);
    }

}

class Blockchain {

    constructor() {
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 5;
        this.pendingTransactions = [];
        this.miningReward = 1;
    }

    createGenesisBlock() {
        return new Block('20/02/2020', 'Genesis block', '0');
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    // mock function - add test block
    addBlock(newBlock) {
        newBlock.previousHash = this.getLatestBlock().hash;
        // newBlock.hash = newBlock.calculateHash(); // no difficulty 
        newBlock.mineBlock(this.difficulty); // use difficulty
        this.chain.push(newBlock);
    }

    minePendingTransactions(miningRewardAddress) {
        let block = new Block(Date.now().toLocaleDateString, this.pendingTransactions);
        block.mineBlock(this.difficulty);

        block.previousHash = this.getLatestBlock().hash;

        console.log('Block successfully mined.');
        this.chain.push(block);

        this.pendingTransactions = [
            new Transaction(null, miningRewardAddress, this.miningReward)
        ];
    }

    createTransaction(transaction) {
        this.pendingTransactions.push(transaction);
    }

    getBalanceOfAddress(address) {
        let balance = 0;

        for (const block of this.chain) {
            for (const trans of block.transactions) {
                if (trans.fromAddress === address) {
                    balance -= trans.amount;
                }

                if (trans.toAddress === address) {
                    balance += trans.amount;
                }
            }
        }

        return balance;
    }

    isChainValid() {
        // skips genesis block
        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            // if (currentBlock.hash !== currentBlock.calculateHash()) {
            //     return false;
            // }

            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }

        return true;
    }

}

class Transaction {
    constructor(fromAddress, toAddress, amount) {
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.amount = amount;
    }
}

/** Test Code */

let prutah = new Blockchain();

prutah.createTransaction(new Transaction('address1', 'address2', 10));
prutah.createTransaction(new Transaction('address2', 'address1', 5));

console.log('Starting miner...');
prutah.minePendingTransactions('devon-address');

console.log('Balance of miner ' + prutah.getBalanceOfAddress('devon-address'));

console.log('Starting miner again...');
prutah.minePendingTransactions('devon-address');

console.log('Balance of miner ' + prutah.getBalanceOfAddress('devon-address'));


// Mock Transactions
console.log('Mining Block 1...');
prutah.addBlock(
    new Block(
        '01/01/2019', { 
            amount: 1 
        }
        ));

console.log('Mining Block 2...');
prutah.addBlock(
    new Block(
        '02/02/2019', { 
            amount: 45
        }
        ));

console.log('Mining Block 3...');
prutah.addBlock(
    new Block(
        '03/02/2019', 
        'test', { 
            amount: 51
        }
        ));

// Output
console.log('Test chain: ');
console.log(JSON.stringify(prutah, null , 4));

console.log('Blockchain is valid: ' + prutah.isChainValid())

// Mock data override
prutah.chain[1].data = { amount: 100 };
prutah.chain[1].hash = prutah.chain[1].calculateHash();

// Output
console.log('Block 1 illegal modified data with recalculated hash:');
console.log(JSON.stringify(prutah, null , 4));

console.log('Blockchain is valid: ' + prutah.isChainValid())